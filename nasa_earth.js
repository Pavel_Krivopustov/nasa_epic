var image_cell_template = "<div class='col-sm-3 image-cell'><div class='nasa-image'><img></div><div class='image-caption'></div><div class='image-coords'></div></div>";
var date_tmpl = "begin=YEAR-MONTH-DAY";
var myApiKey = "api_key=FJMDw1yPw3M7EU8Kjt4qOzLiDDoztRuTlEAjh5tx";

var example_image_url = "https://api.nasa.gov/EPIC/archive/natural/2015/06/13/png/epic_1b_20150613110250_01.png?api_key=FJMDw1yPw3M7EU8Kjt4qOzLiDDoztRuTlEAjh5tx";
var example_image_url_2 = "https://api.nasa.gov/EPIC/archive/natural/2017/08/21/png/epic_1b_20170821151450.png?api_key=FJMDw1yPw3M7EU8Kjt4qOzLiDDoztRuTlEAjh5tx";
var epic_natural_archive_base = "https://api.nasa.gov/EPIC/archive/natural/";
var api_url_query_base = "https://epic.gsfc.nasa.gov/api/natural/date/";
var images = [];
// ==========================================================
// START JS: synchronize the javascript to the DOM loading
// ==========================================================
$(document).ready(function() {

  // ========================================================
  // SECTION 1:  process on click events
  // ========================================================
  $('#get-images-btn').on('click', api_search);

  // process the "future" img element dynamically generated
  $("div").on("click", "img", function(){
    console.log(this.src);
    // call render_highres() and display the high resolution
	render_highres(this.src);
  });

  // ========================================================
  // TASK 1:  build the search AJAX call on NASA EPIC
  // ========================================================
  // Do the actual search in this function
  function api_search(e) {

    // get the value of the input search text box => date
    var date = document.getElementById("search_date").value;
   
    // build an info object to hold the search term and API key
    var info = {};
    var date_array = date.split('-');
    info.year = date_array[0];
	info.month = date_array[1];
    info.day = date_array[2];
    info.api_key = "FJMDw1yPw3M7EU8Kjt4qOzLiDDoztRuTlEAjh5tx";
	

    // build the search url and sling it into the URL request HTML element
    var search_url = "https://epic.gsfc.nasa.gov/api/natural/date/" + info.year + "-" + info.month + "-" + info.day + "?api_key=" + info.api_key;
    // console.log(search_url);
    // sling it!
	document.getElementById("reqURL").innerHTML = search_url;
	

    // make the jQuery AJAX call!
    $.ajax({
      url: search_url,
      success: function(data) {
        render_images(data,info);
      },
      cache: false
    });
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================

  // ========================================================
  // TASK 2: perform all image grid rendering operations
  // ========================================================
  function render_images(data,info) {
    // get NASA earth data from search results data
    // console.log(data.length);
    
	images = [];
	var cenCoordinates = [];
    for (var i = 0; i < data.length; i++) {
      // build an array of objects that captures all of the key image data
      
	  // => image url
	  var img = data[i]["image"];
	  var imgURL = "https://api.nasa.gov/EPIC/archive/natural/" + info.year + "/" + info.month + "/" + info.day + "/png/" + img + ".png?api_key=" + info.api_key;
	  
      // => centroid coordinates to be displayed in the caption area
	  var cenCoordLat = data[i]["centroid_coordinates"]["lat"];
	  var cenCoordLon = data[i]["centroid_coordinates"]["lon"];
	  var centroidCoordinates = "lat:" + cenCoordLat + " lon:" + cenCoordLon;
      
	  // => image date to be displayed in the caption area (under thumbnail)
	  var imgDate = data[i]["date"];
	  // create an object
	  var allData = {url:imgURL, coodinates:centroidCoordinates, imageDate:imgDate};
	  
	  images.push(allData);
	  
    }
   
    //console.log(images[0]["url"]);
	
    // select the image grid and clear out the previous render (if any)
    var earth_dom = $('#image-grid');
    earth_dom.empty();

    // render all images in an iterative loop here!
	for(var i = 0; i < data.length; i++){
		
		var tmp = "<div class=\"row\">";
		
		//console.log(image_cell);
		
		if((i % 4) === 0) {
		    if(i < data.length)
				tmp += "<div class='col-sm-3 image-cell'><div class='nasa-image'><img src="+ images[i]["url"] + "></div><div class='image-caption'>" + images[i]["imageDate"] + "</div><div class='image-coords'>" + images[i]["coodinates"] + "</div></div>";
			i++;
			if(i < data.length)
				tmp += "<div class='col-sm-3 image-cell'><div class='nasa-image'><img src="+ images[i]["url"] + "></div><div class='image-caption'>" + images[i]["imageDate"] + "</div><div class='image-coords'>" + images[i]["coodinates"] + "</div></div>";
			i++;
			if(i < data.length)
				tmp += "<div class='col-sm-3 image-cell'><div class='nasa-image'><img src="+ images[i]["url"] + "></div><div class='image-caption'>" + images[i]["imageDate"] + "</div><div class='image-coords'>" + images[i]["coodinates"] + "</div></div>";
			i++;
			if(i < data.length)
				tmp += "<div class='col-sm-3 image-cell'><div class='nasa-image'><img src="+ images[i]["url"] + "></div><div class='image-caption'>" + images[i]["imageDate"] + "</div><div class='image-coords'>" + images[i]["coodinates"] + "</div></div>";
			// enter a new <div class="row"> 
			earth_dom.append(tmp + "</div>");
		}
		
	}
  }

  // ========================================================
  // TASK 3: perform single high resolution rendering
  // ========================================================
  // function to render the high resolution image
  function render_highres(src_url) {
    // use jQuery to select and maniupate the portion of the DOM => #image-grid
    //  to insert your high resolution image
	var earth_dom_2 = $('#image-grid');
    earth_dom_2.empty();
	earth_dom_2.append("<img class='highres' src=" + src_url + " width=800>");
	
	document.getElementById("imgURL").innerHTML = src_url;
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
});
